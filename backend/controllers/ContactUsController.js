import nodemailer from "nodemailer";


const contactus = (req,res) => {
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.email,
      pass: process.env.pass
    }
  });

  var mailOptions = {
    from: process.env.email,
    to: process.env.email,
    subject: `Greenie - Message from ${req.body.email}`,
    text: `Name : ${req.body.name}`+"\n\n"+req.body.message
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
     res.status(400).json({
        message:error
     })
    } else {
        res.status(200).json({
            message:"email sent"
         })
    }
  });
}

export default contactus
  
