import Category from "../models/categoryModel.js"
import asyncHandler from 'express-async-handler'

//@desc     Get All Categories
//@route    GET /api/category
//@access   Public
const getAllCategories = asyncHandler(async (req, res) => {
    const categories = await Category.find()
    res.status(200).json(categories)
})

//@desc     Get One Categories
//@route    GET /api/category/:id
//@access   Public
const getOneCategory = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id)
  if(!category){
    res.status(400)
    throw new Error("Category not found")
  }
  res.status(200).json(category.name)
})

//@desc     Create Category
//@route    POST /api/category
//@access   Private/admin
const CreateCategory = asyncHandler(async (req, res) => {
    if(!req.body.name){ //Error Handling
       res.status(400)
      throw new Error('Please add a name of the category')
    }
    const category = await Category.create({
      name:req.body.name,
    })
res.status(200).json(category)

})

//@desc     Update Category
//@route    PUT /api/category/id
//@access   Private/admin
const updateCategory =asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id)
    if(!category){
        res.status(400)
        throw new Error('Category not found')
    }

    const updatedCategory = await Category.findByIdAndUpdate(req.params.id, req.body , {new:true,})
    res.status(200).json(updatedCategory)
})

//@desc     Delete Goal
//@route    DELETE /api/goals/id
//@access   Private/admin
const deleteCategory = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id)
    if(!category){
        res.status(400)
        throw new Error('Category not found')
    }
    await category.remove()
    res.status(200).json({id : req.params.id,msg:"Deleted Successfully"})
})

export {
  getAllCategories,
  getOneCategory,
  CreateCategory,
  updateCategory,
  deleteCategory,
};
