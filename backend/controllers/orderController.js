import User from "../models/userModel.js";
import Order from "../models/orderModel.js";
import asyncHandler from "express-async-handler";

// @desc    Create new order
// @route   POST /api/orders
// @access  Private
const addOrderItems = asyncHandler(async (req, res) => {
  const { orderItems, addressDetails } = req.body;
  if (orderItems && orderItems.length === 0) {
    res.status(400);
    throw new Error("No order items");
    return;
  } else {
    var totalPrice = 0;
    for (let i = 0; i < orderItems.length; i++) {
      totalPrice += orderItems[i].price * orderItems[i].qty;
    }
    const order = new Order({
      orderItems,
      user_id: req.user._id,
      addressDetails,
      totalPrice: totalPrice,
    });
    var points = 0;
    for (let i = 0; i < orderItems.length; i++) {
      points += orderItems[i].points * orderItems[i].qty;
    }
    let user = await User.findById(req.user._id)
    user.points+=points;
    await user.save()
    // let updateUser = await User.findByIdAndUpdate(
    //   {
    //     _id: req.user._id,
    //   },
    //   { points:(prev)=>prev+points }
    // );
    // console.log("updated", updateUser);

    const createdOrder = await order.save();

    res.status(201).json(createdOrder);
  }
});

// @desc    Get all orders
// @route   GET /api/orders
// @access  Private/Admin
const getOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({}).populate("user_id", "id name");
  res.json(orders);
});

// @desc    Get order by ID
// @route   GET /api/orders/:id
// @access  Private
const getOrderById = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id).populate(
    "user_id",
    "name email"
  );

  if (order) {
    res.json(order);
  } else {
    res.status(404);
    throw new Error("Order not found");
  }
});

// @desc    Get logged in user orders
// @route   GET /api/orders/myorders
// @access  Private
const getMyOrders = asyncHandler(async (req, res) => {
  const orders = await Order.find({ user_id: req.user._id });
  res.json(orders);
});

const deleteOrder = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id);
  if (!order) {
    res.status(400);
    throw new Error("Order not found");
  }
  await order.remove();
  res.status(200).json({ id: req.params.id, msg: "Deleted Successfully" });
});
 
export { addOrderItems, getOrders, getOrderById, getMyOrders, deleteOrder };
