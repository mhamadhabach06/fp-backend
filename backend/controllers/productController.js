import Product from "../models/productModel.js";
import asyncHandler from "express-async-handler";

//@desc     Get All Products
//@route    GET /api/products
//@access   Public
const getAllProducts = asyncHandler(async (req, res) => {
  const products = await Product.find().populate("category_id");
  res.status(200).json(products);
});

//get products filtered by category
//@route    GET /api/products/productByCat/:id
//@access   Public

const getProductsByCategory = async (req, res) => {
  try {
    const prodByCat = await Product.find({
      category_id: req.params.id,
    }).populate("category_id");
    res.status(200).json(prodByCat);
  } catch (err) {
    res.json({ message: err });
  }
};

//@desc     Get  Product By id
//@route    GET /api/products/:id
//@access   Public
const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    res.status(200).json(product);
  } else {
    res.status(404);
    throw new Error("Product not found");
  }
});

// @desc    Delete a product
// @route   DELETE /api/products/:id
// @access  Private/Admin
const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id);

  if (product) {
    await product.remove();
    res.status(200).json({ message: "Product removed" });
  } else {
    res.status(404);
    throw new Error("Product not found");
  }
});

// @desc    Create a product
// @route   POST /api/products
// @access  Private/Admin
const createProduct = asyncHandler(async (req, res) => {
  const images = [];
  for (let i = 0; i < req.files.length; i++) {
    let file = `https://plantify-api.onrender.com/${req.files[i].path}`;
    images.push(file);
  }
  const product = new Product({
    name: req.body.name,
    stock_qty: req.body.stock_qty,
    file: images,
    points: req.body.points,
    price: req.body.price,
    desc: req.body.desc,
    category_id:req.body.category_id
  });

  if (!product) {
    res.status(400).json({ message: err });
  }

  const createdProd = await product.save();
  res.status(201).json(createdProd);
});

// @desc    Update a product
// @route   PUT /api/products/:id
// @access  Private/Admin
const updateProduct = asyncHandler(async (req, res) => {
  // const product = await Product.findById(req.params.id);
  // if (!product) {
  //   res.status(400);
  //   throw new Error("Product not found");
  // }

  // const updatedProduct = await Product.findByIdAndUpdate(
  //   req.params.id,
  //   req.body,
  //   { new: true }
  // );
  // res.status(200).json(updatedProduct);
  let updated = {};
  if (req.body.name) {
    updated.name = req.body.name;
  }
  if (req.body.stock_qty) {
    updated.stock_qty = req.body.stock_qty;
  }
  if (req.files.length > 0) {
    let images = [];
    for (let i = 0; i < req.files.length; i++) {
      let file = `https://plantify-api.onrender.com/${req.files[i].path}`;
      images.push(file);
    }
    updated.file = images;
  }
  if (req.body.price) {
    updated.price = req.body.price;
  }
  if (req.body.points) {
    updated.points = req.body.points;
  }
  if (req.body.desc) {
    updated.desc = req.body.desc;
  }
 
  try {
    const updatedProduct = await Product.findByIdAndUpdate(
      { _id: req.params.id },
      updated
    );
    res.status(200).json(updatedProduct);
  } catch (err) {
    res.json({ message: err });
  }

});

export {
  getAllProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct,
  getProductsByCategory
};
