import  jwt from "jsonwebtoken"
import bcrypt from "bcryptjs";
import asyncHandler from "express-async-handler";
import User from "../models/userModel.js";


// @desc Register new user
// @route POST /api/users
// @access Public
const registerUser = asyncHandler( async(req,res) =>{
    const {name,email,password,isAdmin} = req.body
    if(!name || !email || !password){
        res.status(400)
        throw new Error('Please add all fields')
    }

    //check if user exists
    const userExists = await User.findOne({email})
    if(userExists){
        res.status(400)
        throw new Error("User already exists")
    }

    //Hashing password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword  = await bcrypt.hash(password,salt)

    //Create user
    const user = await User.create({
        name,
        email,
        password:hashedPassword,
        isAdmin
    })

    if(user){
        res.status(201).json({
            _id :user.id,
            name:user.name,
            email:user.email,
            isAdmin:user.isAdmin,
            token:generateToken(user._id)
        })
    }else{
        res.status(400)
        throw new Error('Invalid user data')
    }


    res.json({message:'Register User'})
})

// @desc Authenticate a user
// @route POST /api/users/login
// @access Public
const loginUser = asyncHandler( async(req,res) =>{
    const {email,password} = req.body

    //check for user email
    const user = await User.findOne({email})

    if(user && (await bcrypt.compare(password,user.password))){
        res.json({
            _id :user.id,
            name:user.name,
            email:user.email,
            token:generateToken(user._id)
        })
    }else{
        res.status(400)
        throw new Error('Invalid credentials')
    }
})

// @desc Get user data (Logged in user )
// @route GET /api/users/me 
// @access Private
const getMe =asyncHandler( async(req,res) =>{
    const{_id ,name ,email,points} = await User.findById(req.user.id)
    res.status(200).json({
        id:_id,
        name,
        email,
        points
    })
})

//Generate JWT
const generateToken = (id) => {
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn:'30d'
    })
}

// @desc    Get user profile
// @route   GET /api/users/profile
// @access  Private
const getUserProfile = asyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id)
  
    if (user) {
      res.json({
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
      })
    } else {
      res.status(404)
      throw new Error('User not found')
    }
  })

// @desc Delete user
// @route GET /api/users/delete/:id 
// @access private/admin
const deleteUser = asyncHandler(async(req,res) =>{
    const user = await User.findById(req.params.id)
    if(!user){
        res.status(400)
        throw new Error('User not found')
    }
    await user.remove()
    res.status(200).json({id : req.params.id,msg:"Deleted Successfully"})
})


// @desc Get All users
// @route GET /api/users/getAllUsers 
// @access private/admin
const getAllUsers = asyncHandler(async(req,res)=>{
    const users = await User.find()
    res.status(200).json(users)
})



export {
    registerUser,
    loginUser,
    getMe,
    deleteUser,
    getAllUsers,
    getUserProfile
} ;