import mongoose from "mongoose";

const { Schema } = mongoose;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: [true, "please add the Category name"],
    min: 1,
    max: 50,
  },
});

export default mongoose.model("Category", CategorySchema);
