import mongoose from "mongoose";

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please add a name"],
      trim: true,
    },

    stock_qty: {
      type: Number,
      required: [true, "Please add a stock quantity"],
      default: 0,
    },

    file: {
      type: [],
    },

    price: {
      type: Number,
      required: [true, "Please add a stock quantity"],
      default: 0,
    },

    points: {
      type: Number,
      required: [true, "Please add points"],
      default: 0,
    },

    desc: {
      type: String,
      required: [true, "Please add a description"],
    },

    category_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category"
      
    },
  },
  { timestamps: true }
);

export default mongoose.model("Product", productSchema);
