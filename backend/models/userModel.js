import mongoose from "mongoose";

const validateEmail = (email) => {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please add a name"],
    trim:true
  },
  email: {
    type: String,
    required: [true, "Please add an email"],
    unique: true,
    trim:true,
    validate: [validateEmail, "Please fill a valid email address"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please fill a valid email address",
    ],
    
  },
  password: {
    type: String,
    required: [true, "Please add a password"],
  },
  isAdmin: {
    type: Boolean,
    default:false
  },
  points: {
    type:Number,
    default:0
  }
},{timestamps:true});  

export default mongoose.model('User',userSchema);
