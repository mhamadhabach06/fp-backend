import express from "express";
const ContactUsRouter = express.Router();
import contactus from "../controllers/ContactUsController.js"

ContactUsRouter.route('/contactus').post(contactus)

export default ContactUsRouter