import express from "express";
const CategoryRouter = express.Router();
import {
  getAllCategories,
  getOneCategory,
  CreateCategory,
  updateCategory,
  deleteCategory,
} from "../controllers/categoryController.js";
import { admin, protect } from "../middlewares/authMiddleware.js";

CategoryRouter.route('/').get(getAllCategories).post(protect,admin,CreateCategory)
CategoryRouter.route('/:id').put(protect,admin,updateCategory).delete(protect,admin,deleteCategory).get(getOneCategory)

export default CategoryRouter;
