import express from "express";
const orderRouter = express.Router();

import {addOrderItems, deleteOrder, getMyOrders, getOrderById, getOrders} from "../controllers/orderController.js"
import { admin, protect } from "../middlewares/authMiddleware.js";

orderRouter.route('/').post(protect,addOrderItems).get(protect,admin,getOrders)
orderRouter.route('/:id').post(protect,getOrderById)
orderRouter.route('/myorders').get(protect,getMyOrders)
orderRouter.route('/deleteOrder/:id').delete(protect,deleteOrder)

export default orderRouter;
