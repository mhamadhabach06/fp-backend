import express from "express";
import  {createProduct, deleteProduct, getAllProducts, getProductById, getProductsByCategory, updateProduct}  from "../controllers/productController.js";
import { admin, protect } from "../middlewares/authMiddleware.js";
import { upload } from "../middlewares/multer.js";

const productRouter = express.Router()

productRouter.get('/',getAllProducts).post('/',protect,admin,upload.array("file"),createProduct)
productRouter.get('/:id',getProductById).delete('/:id',protect,admin,deleteProduct).put('/:id',protect,admin,upload.array("file"),updateProduct)
productRouter.get('/productByCat/:id',getProductsByCategory)
export default productRouter;
