import express from "express";
import {registerUser,getMe,loginUser, deleteUser, getAllUsers, getUserProfile} from "../controllers/userController.js"
import {admin, protect} from "../middlewares/authMiddleware.js";

const userRouter = express.Router()

userRouter.post('/',registerUser)
userRouter.post('/login',loginUser)
userRouter.get('/me',protect,getMe)
userRouter.delete('/delete/:id',protect,admin,deleteUser)
userRouter.get('/getAllUsers',protect,admin,getAllUsers)
userRouter.get('/userProfile',protect,getUserProfile)




export default userRouter;
