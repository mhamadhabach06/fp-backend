import express from "express";
import "dotenv/config";
import bodyParser from "body-parser";
import errorHandler from "./middlewares/errorMiddleware.js";
import { connectDB } from "./config/db.js";
import userRouter from "./routes/userRoutes.js";
import productRouter from "./routes/productRoutes.js";
import CategoryRouter from "./routes/categoryRouters.js";
import orderRouter from "./routes/orderRoutes.js";
import cors from "cors";
import ContactUsRouter from "./routes/ContactUsRoutes.js";

const port = process.env.PORT || 5000

connectDB()

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(errorHandler)


app.use("/upload", express.static("upload"));
app.use(cors());
app.use("/api/users",userRouter)
app.use("/api/products",productRouter)
app.use("/api/category", CategoryRouter);
app.use("/api/orders", orderRouter);
app.use("/api",ContactUsRouter)


// console.log(userRouter)
app.listen(port,() => console.log(`Server started on port ${port}`))

 